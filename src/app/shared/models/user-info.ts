export class UserInfo {
    roles:Array<string>;
    name: string;
    email:string;
    authToken:string;
}
